\baselineskip =0.7\baselineskip 
\contentsline {chapter}{List of Tables}{vii}{chapter*.2}
\contentsline {chapter}{List of Figures}{viii}{chapter*.3}
\contentsline {chapter}{Abstract}{x}{chapter*.4}
\contentsline {chapter}{List of Abbreviations and Symbols Used}{xi}{chapter*.5}
\contentsline {chapter}{Acknowledgements}{xii}{chapter*.6}
\contentsline {chapter}{\numberline {Chapter 1}Introduction}{1}{chapter.1}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {1.1}The Voting Problem}{2}{section.1.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {1.2}Coercion-Resistant Elections}{2}{section.1.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {1.3}Contribution of this Thesis}{3}{section.1.3}
\addvspace {10\p@ }
\contentsline {section}{\numberline {1.4}Outline of the Thesis}{3}{section.1.4}
\contentsline {chapter}{\numberline {Chapter 2}Background}{4}{chapter.2}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {2.1}A Peek Into Democracy and Voting}{4}{section.2.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {2.2}What Makes Voting Hard?}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Lack of Trust}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Evidence and Receipts}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Secrecy}{7}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Auditable Tallying}{7}{subsection.2.2.4}
\addvspace {10\p@ }
\contentsline {section}{\numberline {2.3}Technology and Elections}{8}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Optical Scanners}{8}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Direct Record Electronic Voting Machine}{8}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Ballot Marking Devices}{9}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}Internet and Remote Voting}{9}{subsection.2.3.4}
\contentsline {chapter}{\numberline {Chapter 3}Literature Survey}{10}{chapter.3}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.1}Internet Voting Schemes and Protocols}{10}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Selene}{10}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Civitas}{11}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Helios}{11}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Pr{\^e}t {\`a} Voter}{12}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Scantegrity II}{12}{subsection.3.1.5}
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.2}Extending Receipt Freeness}{13}{section.3.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.3}The JCJ Protocol}{13}{section.3.3}
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.4}Civitas}{16}{section.3.4}
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.5}Proposed Improvements to the JCJ Protocol}{18}{section.3.5}
\contentsline {subsubsection}{Suggestions by Smith and Weber}{19}{section*.9}
\contentsline {subsubsection}{Suggestions by Araujo et al.}{19}{section*.10}
\contentsline {subsubsection}{Selections}{20}{section*.11}
\contentsline {subsubsection}{Suggestions by Spycher et al.}{20}{section*.12}
\addvspace {10\p@ }
\contentsline {section}{\numberline {3.6}Research Objectives}{21}{section.3.6}
\contentsline {chapter}{\numberline {Chapter 4}The Proposed Protocol}{22}{chapter.4}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.1}Overview of the Process}{22}{section.4.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.2}Players}{24}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Supervisor}{24}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Authentication Teller}{24}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Registration Teller}{25}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4} Voter}{25}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Coercer}{26}{subsection.4.2.5}
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.3}Setup Phases}{26}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}System Setup Phase}{26}{subsection.4.3.1}
\contentsline {subsubsection}{Supervisor Setup}{26}{section*.15}
\contentsline {subsubsection}{Voter Authentication}{27}{section*.16}
\contentsline {subsection}{\numberline {4.3.2}Election Phase}{27}{subsection.4.3.2}
\contentsline {subsubsection}{Election Creation Phase}{27}{section*.17}
\contentsline {subsubsection}{Registration Phase}{28}{section*.18}
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.4}Indexed Bulletin Board}{28}{section.4.4}
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.5}Ballot Casting}{29}{section.4.5}
\addvspace {10\p@ }
\contentsline {section}{\numberline {4.6}Tallying}{30}{section.4.6}
\contentsline {chapter}{\numberline {Chapter 5}Verifying the Proposed Protocol}{31}{chapter.5}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {5.1}Overview of ProVerif and Applied Pi Calculus}{31}{section.5.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {5.2}Syntax of Applied Pi Calculus}{32}{section.5.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {5.3}Modeling and Verifying the Protocol}{33}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Soundness}{33}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Coercion Resistance}{35}{subsection.5.3.2}
\contentsline {chapter}{\numberline {Chapter 6}Algorithms in the Proposed Protocol}{41}{chapter.6}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {6.1}Concepts in Cryptographic Systems}{41}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Public Key Cryptography}{41}{subsection.6.1.1}
\contentsline {subsubsection}{The Discrete Logarithm Problem}{42}{section*.22}
\contentsline {subsubsection}{Parameter Generation}{42}{section*.23}
\contentsline {subsubsection}{Key Generation}{43}{section*.25}
\contentsline {subsubsection}{Encode}{44}{section*.27}
\contentsline {subsubsection}{Encryption}{45}{section*.29}
\contentsline {subsubsection}{Re-encryption}{45}{section*.31}
\contentsline {subsubsection}{Decryption}{46}{section*.33}
\contentsline {subsection}{\numberline {6.1.2}Commitments and Hashes}{48}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Zero Knowledge Proofs}{49}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Distributed Cryptosystem}{50}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Homomorphic Encryption}{54}{subsection.6.1.5}
\contentsline {subsection}{\numberline {6.1.6}Mixnets}{55}{subsection.6.1.6}
\addvspace {10\p@ }
\contentsline {section}{\numberline {6.2}Managing Credentials in Resist}{57}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Registration}{57}{subsection.6.2.1}
\contentsline {subsubsection}{Credential Generation}{57}{section*.42}
\contentsline {subsubsection}{Credential Encryption}{59}{section*.43}
\contentsline {subsubsection}{Credential Verification}{59}{section*.45}
\contentsline {subsubsection}{Digital Verifier Reencryption Proof}{59}{section*.47}
\contentsline {subsubsection}{Obtaining Credential Shares}{61}{section*.49}
\contentsline {subsection}{\numberline {6.2.2}Fake Credentials}{61}{subsection.6.2.2}
\contentsline {subsubsection}{Creating Fake Credentials}{64}{section*.51}
\addvspace {10\p@ }
\contentsline {section}{\numberline {6.3}Casting a Vote}{64}{section.6.3}
\addvspace {10\p@ }
\contentsline {section}{\numberline {6.4}Tabulation}{68}{section.6.4}
\contentsline {chapter}{\numberline {Chapter 7}System Implementation}{73}{chapter.7}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {7.1}The Django Framework}{73}{section.7.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {7.2}PostgreSQL as a Database}{74}{section.7.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {7.3}Languages}{75}{section.7.3}
\contentsline {subsubsection}{Python}{75}{section*.59}
\contentsline {subsubsection}{VanillaJS}{76}{section*.60}
\contentsline {subsubsection}{Cryptographic Libraries}{76}{section*.61}
\addvspace {10\p@ }
\contentsline {section}{\numberline {7.4}Object Notations}{77}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}JSON}{77}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Integers, BigIntegers, and Arrays}{80}{subsection.7.4.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {7.5}Modeling Theoretical Concepts}{81}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Users, Privileges, and Groups}{81}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}Implementing the Bulletin Board}{82}{subsection.7.5.2}
\contentsline {subsection}{\numberline {7.5.3}Supervisor}{83}{subsection.7.5.3}
\contentsline {subsection}{\numberline {7.5.4}Voter Lists}{83}{subsection.7.5.4}
\contentsline {subsection}{\numberline {7.5.5}Registration Tellers}{84}{subsection.7.5.5}
\contentsline {subsection}{\numberline {7.5.6}Casting a Vote}{85}{subsection.7.5.6}
\contentsline {subsection}{\numberline {7.5.7}Tabulation Tellers}{86}{subsection.7.5.7}
\contentsline {chapter}{\numberline {Chapter 8}Evaluation}{104}{chapter.8}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {8.1}By Number of Modular Exponentiations}{104}{section.8.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {8.2}Timing Measurements}{106}{section.8.2}
\contentsline {chapter}{\numberline {Chapter 9}Conclusion and Future Work}{108}{chapter.9}
\addvspace {10\p@ }
\addvspace {10\p@ }
\contentsline {section}{\numberline {9.1}Conclusion}{108}{section.9.1}
\addvspace {10\p@ }
\contentsline {section}{\numberline {9.2}Limitations}{109}{section.9.2}
\addvspace {10\p@ }
\contentsline {section}{\numberline {9.3}Extensions}{109}{section.9.3}
\contentsline {chapter}{Bibliography}{111}{chapter*.84}
